#include <ConnectionN.h>



void remove_duplicate_connections(vector<ConnectionN*> &con)
{
    bool change = true;
    while(change)
    {
    change = false;
    for(int i=0;i<con.size();i++)
    {


        for(int j=i;j<con.size();j++)
        {
            //cout << i << " " << j << endl;
            if(same_lines(*con[i],*con[j])&&i!=j)
            {
                //cout << "if for same lines" << endl;
                if((con[i]->stop_numbers) > (con[j]->stop_numbers))
                    con.erase(con.begin()+i);
                else
                    con.erase(con.begin()+j);
                change = true;
                break;
                break;
            }


            }}
    }
};


bool same_lines(ConnectionN& a, ConnectionN& b)
{
    if(a.con.size()!=b.con.size())
        return false;
    for(int i=0;i<a.con.size();i++)
        if((a.con[i])->line != (b.con[i])->line)
        return false;
    return true;
};

bool operator == (ConnectionN& a, ConnectionN& b)
{
    if(a.con.size() !=b.con.size())
        return false;
    for(int i=0;i<a.con.size();i++)
        if(*a.con[i] != *b.con[i])
        return false;
    return true;
}
