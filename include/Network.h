// *** ADDED BY HEADER FIXUP ***
#include <istream>
// *** END ***
#ifndef NETWORK_H
#define NETWORK_H
#include "Line.h"
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <Connection.h>
#include <ConnectionN.h>
#include <Journey.h>
#include <JourneyN.h>




class Network
{
    public:

        vector <Line*> lines;
        //all stops
        vector <Stop*> stops;
        //describes the stops where for changing
        vector <Stop*> change_stops;
        //store Connections from first coordinate Stop i from StopList to Stop j from StopList
        vector<vector<vector<ConnectionN*>>> connection_list;
        vector<vector<vector<ConnectionN*>>> connection2_list;
        vector<vector<vector<ConnectionN*>>> connection3_list;
        vector<vector<vector<ConnectionN*>>> connection4_list;

        /** Default constructor */
        Network();
        /** Default destructor */
        virtual ~Network();
        /** Copy constructor
         *  \param other Object to copy from
         */
        Network(const Network& other);
        /** Assignment operator
         *  \param other Object to assign from
         *  \return A reference to this
         */
        Network& operator=(const Network& other);

        void create_connection_list();

        void create_2connection_list();

        void create_3connection_list();

        void create_4connection_list();

        void create_line(string filename);

        void create_stops(string filename);

        void read_busline(string name);

        Stop* get_stop(int Id);

        Stop* get_stop(string name);

        Line* get_line (int number);

        vector<ConnectionN*> find_connection(Stop* start, Stop* endd);

        vector<ConnectionN*> find_2connection(Stop* start, Stop* endd);

        vector<ConnectionN*> find_3connection(Stop* start, Stop* endd);

        vector<ConnectionN*> find_4connection(Stop* start, Stop* endd);

        //vector<ConnectionN*> find_3connection_second(Stop* start, Stop* endd);

        vector<ConnectionN*>& get_connection_from_table(Stop* start, Stop* endd);

        vector<ConnectionN*>& get_2connection_from_table(Stop* start, Stop* endd);

        vector<ConnectionN*>& get_3connection_from_table(Stop* start, Stop* endd);

        vector<ConnectionN*>& get_4connection_from_table(Stop* start, Stop* endd);

        vector<JourneyN*> find_journeys (Stop* start, Stop* endd, Time_mh depart);

    protected:

    private:
};

#endif // NETWORK_H
