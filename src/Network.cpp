#include "Network.h"

using namespace std;

Network::Network()
{
    //ctor
}

Network::~Network()
{
    //dtor
}

Network::Network(const Network& other)
{
    //copy ctor
}

Network& Network::operator=(const Network& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    return *this;
}

void Network::create_stops(string filename)
{
    cout << "create_stops starting " << filename << endl;
    fstream is;
    is.open(filename, ios::in);
    string line = "";
    string word;
    int row_number=0;
    int stop_index=0;
     while (getline(is, line)) {
        if (line.substr(0,1)=="#")
        {
            continue;
        }
        row_number++;
        cout << row_number << endl;
        stringstream s(line);
        getline(s, word, ';');
        //cout << word << endl;
        Stop* _stop = new Stop;
        _stop->Name = word;
        _stop->Id = stop_index;
        stop_index++;
        getline(s,word,';');
        //cout << word << endl;
        _stop->change_time = stoi(word);
        getline(s,word,';');
        _stop->change_stop = stoi(word);
        if(  getline(s,word,';'))
        {
            cout << "network::create_stops error" << endl;
            std::exit(99);
        }
        stops.push_back(_stop);
        if(_stop->change_stop==1)
            change_stops.push_back(_stop);
        //cout << "stop name " << stops[row_number-1]->Name << endl;
     }
     cout << "create_stops finishing " << filename << endl;




}

void Network::create_line(std::string filename)
{
    cout << "create_line starting " << filename << endl;

    fstream is;
    is.open(filename, ios::in);
    Line* _line = new Line();
    string line = "";
    int row_number= 0;
    while (getline(is, line)) {
        //cout << line << endl;
        if (line.substr(0,1)=="#")
        {
            continue;
        }
        row_number++;
        Route* _route = new Route();

        stringstream s(line);
        string word = {};
       if (row_number==1)
        {
            //cout << "if " << row_number << endl;
            getline(s,word,';');
            _line->number = stoi(word);
            getline(s,word,';');
            _line->shield_number = stoi(word);
        }
        else if(row_number==2)
        {
            getline(s,word,';');
            getline(s,word,';');
            getline(s,word,';');
            getline(s,word,';');
            while(getline(s,word,';'))
            {
                Stop* stop = new Stop(word);
                for(int i=0;i<stops.size();i++)
                {

                    if(stops[i]->Name == word)
                {
                    //cout<< stops[i]->Name << endl;
                    stops[i]->line_numbers.push_back(_line->number);
                    stop = stops[i];
                    break;
                }
                    if(i==stops.size()-1)
                    {

                        cout << "Network::create_line " << filename << " stops error, stop not found, with stop " << word << endl;
                        std::exit(99);
                    }
                }
                _line->StopList.push_back(stop);

            }

        }
        else
        {
            getline(s,word,';');
             _route->workday = stoi(word);
             getline(s,word,';');
             _route->saturday = stoi(word);
             getline(s,word,';');
             _route->sunday = stoi(word);
             getline(s,word,';');
             _route->vacation = stoi(word);
             vector <string> columns;
        while( getline(s,word,';'))
        {
            columns.push_back(word);
        }
        for(int i=0;i<columns.size();i++)
        {
            //cout << columns[i] << endl;
           Time_mh t(columns[i]);
           _route->dep.push_back(t);
        }
        //_route->comment = columns[columns.size()-1];
        _line->routes.push_back(_route);
        _route->route_number=(_line->routes).size()-1;
        }


    }
    lines.push_back(_line);
     cout << "create_line finished " << filename << endl;
}

Stop* Network::get_stop(int Id)
{
    for(int i=0;i<(stops).size();i++)
        if(Id == stops[i]->Id)
        {
            return stops[i];
        }
        return NULL;
    std::cout << "Stop not found" << endl;
    std::exit(92);
}

Stop* Network::get_stop(string name)
{
    for(int i=0;i<(stops).size();i++)
        if(name == stops[i]->Name)
            return stops[i];
    std::cout << "Stop not found" << endl;
    std::exit(92);
}

 Line* Network::get_line (int number)
 {
        for(int i=0;i<(lines).size();i++)
        if(number == lines[i]->number)
            return lines[i];
    std::cout << "Stop not found" << endl;
    std::exit(92);
 }

 vector<ConnectionN*> Network::find_connection(Stop* start, Stop* endd)
 {
     vector <ConnectionN*> con_lines;
     for(int i=0;i<lines.size();i++)
     {

         Line *l = new Line();
         *l= *lines[i];
         int st, ed;
         ed=0;
         st=100;
         for(int j=0;j<l->StopList.size();j++)
         {

            if(l->StopList[j]==start)
            {
                st=j;
            }
            if(l->StopList[j]==endd)
            {
                ed = j;
                break;
            }
         }
         if(st<ed)
         {
             //cout << "if connection creating" << endl;
              Connection *c = new Connection(start, endd, lines[i]);
              ConnectionN *cc = new ConnectionN();
              cc->push_connection(c);
              con_lines.push_back(cc);
         }


     }
    return con_lines;
 }

 vector<ConnectionN*> Network::find_2connection (Stop* start, Stop* endd)
 {
     vector<Stop*> stops_temp = stops;
     vector<ConnectionN*> conN;
     for(int i=stops_temp.size()-1;i>=0;i--)
        if(stops_temp[i]->change_stop==0 || stops_temp[i]==start || stops_temp[i]==endd)
     {
       // iterator it=i;
            stops_temp.erase(stops_temp.begin()+i);
            }
            //cout << stops_temp.size() << endl;
    //for(int i=0;i<stops_temp.size();)
     for(int i=0;i<stops_temp.size();i++)
 {
        vector <ConnectionN*> c1 = get_connection_from_table(start, stops_temp[i]);//find_connection(start,stops_temp[i]);
        vector <ConnectionN*> c2 = get_connection_from_table(stops_temp[i],endd);//find_connection(stops_temp[i], endd);
        for(int j=0;j<c1.size();j++)
            for(int k=0;k<c2.size();k++)
        {
            if((c1[j]->con[0]->line) != (c2[k]->con[0]->line) && !(c1[j]->con[0]->goes_through(endd))&& !(c2[k]->con[0]->goes_through(start)) &&
               !(c1[j]->con[0]->goes_to(endd)) && !(c2[k]->con[0]->goes_from(start)))
            {
                ConnectionN* cN = new ConnectionN();
                cN->push_connection(c1[j]->con[0]);
                cN->push_connection(c2[k]->con[0]);
                conN.push_back(cN);
            }

        }}
     remove_duplicate_connections(conN);
     return conN;
 }

void Network::create_connection_list()
{
    cout << "Creating connection list started..." << endl;
    int ssize = (this->stops).size();
    vector<vector<vector<ConnectionN*>>> c (ssize);
    //cout << c.size() << endl;
    for(int i=0;i<c.size();i++)
        c[i]=vector<vector<ConnectionN*>> (ssize);
       // cout << c[0].size() <<endl;
        //c[12][23];

    for(int i=0;i<c.size();i++)
        for(int j=0;j<c.size();j++)
            if(i!=j)
            {
                c[i][j] = this->find_connection(this->stops[i],this->stops[j]);
               /* if(c[i][j].size()!=0)
                cout << i <<" " <<j<< " "<<   c[i][j][0]->line->number << endl;*/
            }
    connection_list = c;
    cout << "Creating connection list finished" << endl;
}

void Network::create_2connection_list()
{
    cout << "Creating 2 connection list started..." << endl;
    int ssize = (this->stops).size();
    vector<vector<vector<ConnectionN*>>> c (ssize);
    //cout << c.size() << endl;
    for(int i=0;i<c.size();i++)
        c[i]=vector<vector<ConnectionN*>> (ssize);
       // cout << c[0].size() <<endl;
        //c[12][23];

    for(int i=0;i<c.size();i++)
        for(int j=0;j<c.size();j++)
            if(i!=j)
            {
                c[i][j] = this->find_2connection(this->stops[i],this->stops[j]);
                //if(c[i][j].size()!=0)
                //cout << i <<" " <<j<< " "<<   c[i][j][0]->line->number << endl;
            }
    connection2_list = c;
    cout << "Creating 2 connection list finished" << endl;
}

 vector<ConnectionN*> Network::find_3connection(Stop* start, Stop* endd)
 {
     vector<ConnectionN*> c1;
     c1.reserve(1000);
     vector<ConnectionN*> c2;
     c2.reserve(1000);
     vector<ConnectionN*> output1;
     output1.reserve(1000);
    for(int i=0;i<change_stops.size();i++)
    {
        if(change_stops[i]!=start && change_stops[i]!=endd)
        {
       c1 = get_connection_from_table(start,change_stops[i]);
       c2 = get_2connection_from_table(change_stops[i],endd);
        }
    for(int k=0;k<c1.size();k++)
        for(int l=0;l<c2.size();l++)
        {
            ConnectionN* conN = new ConnectionN();
            conN->push_connection(c1[k]->con[0]);
            conN->push_connection(c2[l]->con[0]);
            conN->push_connection(c2[l]->con[1]);
            output1.push_back(conN);
        }

    }
    vector<ConnectionN*> d2;
    d2.reserve(1000);
     vector<ConnectionN*> d1;
     d1.reserve(1000);
     vector<ConnectionN*> output2;
     output2.reserve(1000);
    for(int i=0;i<change_stops.size();i++)
    {
        if(change_stops[i]!=start && change_stops[i]!=endd)
        {
       d1 = get_2connection_from_table(start,change_stops[i]);
       d2 = get_connection_from_table(change_stops[i],endd);
        }
    for(int k=0;k<d2.size();k++)
        for(int l=0;l<d1.size();l++)
        {
            ConnectionN* conN = new ConnectionN();
            conN->push_connection(d1[l]->con[0]);
            conN->push_connection(d1[l]->con[1]);
            conN->push_connection(d2[k]->con[0]);
            output2.push_back(conN);
        }

    }
    vector <ConnectionN*> result;
   for(int i=0;i<output1.size();i++)
        for(int j=0;j<output2.size();j++)
            if(*output1[i]==*output2[j])
        {
            result.push_back(output1[i]);
            break;
        }


    return result;
 }

 vector<ConnectionN*> Network::find_4connection(Stop* start, Stop* endd)
 {
     vector<ConnectionN*> c1;
     c1.reserve(1000);
     vector<ConnectionN*> c2;
     c2.reserve(1000);
     vector<ConnectionN*> output1;
     output1.reserve(1000);
    for(int i=0;i<change_stops.size();i++)
    {
        if(change_stops[i]!=start && change_stops[i]!=endd)
        {
       c1 = get_connection_from_table(start,change_stops[i]);
       c2 = get_3connection_from_table(change_stops[i],endd);
        }
    for(int k=0;k<c1.size();k++)
        for(int l=0;l<c2.size();l++)
        {
            ConnectionN* conN = new ConnectionN();
            conN->push_connection(c1[k]->con[0]);
            conN->push_connection(c2[l]->con[0]);
            conN->push_connection(c2[l]->con[1]);
            conN->push_connection(c2[l]->con[2]);
            output1.push_back(conN);
        }

    }
    vector<ConnectionN*> d2;
    d2.reserve(1000);
     vector<ConnectionN*> d1;
     d1.reserve(1000);
     vector<ConnectionN*> output2;
     output2.reserve(1000);
    for(int i=0;i<change_stops.size();i++)
    {
        if(change_stops[i]!=start && change_stops[i]!=endd)
        {
       d1 = get_3connection_from_table(start,change_stops[i]);
       d2 = get_connection_from_table(change_stops[i],endd);
        }
    for(int k=0;k<d2.size();k++)
        for(int l=0;l<d1.size();l++)
        {
            ConnectionN* conN = new ConnectionN();
            conN->push_connection(d1[l]->con[0]);
            conN->push_connection(d1[l]->con[1]);
            conN->push_connection(d1[l]->con[2]);
            conN->push_connection(d2[k]->con[0]);
            output2.push_back(conN);
        }

    }
    vector <ConnectionN*> result;
   for(int i=0;i<output1.size();i++)
        for(int j=0;j<output2.size();j++)
            if(*output1[i]==*output2[j])
        {
            result.push_back(output1[i]);
            break;
        }


    return result;
 }


 void Network::create_3connection_list()
 {
     cout << "Creating 3 connection list started..." << endl;
     int ssize = (this->stops).size();
    vector<vector<vector<ConnectionN*>>> c (ssize);
    //cout << c.size() << endl;
    for(int i=0;i<c.size();i++)
        c[i]=vector<vector<ConnectionN*>> (ssize);
       // cout << c[0].size() <<endl;
        //c[12][23];


    for(int i=0;i<c.size();i++)
        for(int j=0;j<c.size();j++)
            if(i!=j)
            {
                //cout << stops[i]->Name << " " << stops[j]->Name << endl;
                c[i][j] = this->find_3connection(this->stops[i],this->stops[j]);
                //if(c[i][j].size()!=0)
                //cout << i <<" " <<j<< " "<<   c[i][j][0]->line->number << endl;
            }
    connection3_list = c;
    cout << "Creating 3 connection list finished" << endl;
 }

  void Network::create_4connection_list()
 {
     cout << "Creating 4 connection list started..." << endl;
     int ssize = (this->stops).size();
    vector<vector<vector<ConnectionN*>>> c (ssize);
    //cout << c.size() << endl;
    for(int i=0;i<c.size();i++)
        c[i]=vector<vector<ConnectionN*>> (ssize);
       // cout << c[0].size() <<endl;
        //c[12][23];


    for(int i=0;i<c.size();i++)
        for(int j=0;j<c.size();j++)
            if(i!=j)
            {
                //cout << stops[i]->Name << " " << stops[j]->Name << endl;
                c[i][j] = this->find_4connection(this->stops[i],this->stops[j]);
                //if(c[i][j].size()!=0)
                //cout << i <<" " <<j<< " "<<   c[i][j][0]->line->number << endl;
            }
    connection4_list = c;
    cout << "Creating 4 connection list finished..." << endl;
 }

 vector<ConnectionN*>& Network::get_connection_from_table(Stop* start, Stop* endd)
 {
     return connection_list[start->Id][endd->Id];
 }

 vector<ConnectionN*>& Network::get_2connection_from_table(Stop* start, Stop* endd)
 {
     return connection2_list[start->Id][endd->Id];
 }

 vector<ConnectionN*>& Network::get_3connection_from_table(Stop* start, Stop* endd)
 {
     return connection3_list[start->Id][endd->Id];
 }

 vector<ConnectionN*>& Network::get_4connection_from_table(Stop* start, Stop* endd)
 {
     return connection4_list[start->Id][endd->Id];
 }

 vector<JourneyN*> Network::find_journeys (Stop* start, Stop* endd, Time_mh depart)
 {
    cout <<"Searching for journeys from " << start->Name << " to " << endd->Name << endl;
    vector<JourneyN*> result;
    vector<ConnectionN*> a1, a2, a3, a4;
    a1= get_connection_from_table(start, endd);
    a2= get_2connection_from_table(start, endd);
    a3= get_3connection_from_table(start, endd);
    a4= get_4connection_from_table(start, endd);
    for(int i=0;i<a1.size();i++)
    {
        JourneyN * j = new JourneyN(*a1[i],depart);
        result.push_back(j);
    }
    for(int i=0;i<a2.size();i++)
    {
        JourneyN * j = new JourneyN(*a2[i],depart);
        result.push_back(j);
    }
    for(int i=0;i<a3.size();i++)
    {
        JourneyN * j = new JourneyN(*a3[i],depart);
        result.push_back(j);
    }
    for(int i=0;i<a4.size();i++)
    {
        JourneyN * j = new JourneyN(*a4[i],depart);
        result.push_back(j);
    }
    return result;
 }
