#ifndef JOURNEYN_H
#define JOURNEYN_H
#include <vector>
#include <Journey.h>
#include <Connection.h>
#include <Time_mh.h>
#include <ConnectionN.h>
#include <Journey.h>
//#include <functions2.h>


using namespace std;

class JourneyN
{
    public:
        /** Default constructor */
        JourneyN();
        JourneyN(ConnectionN & con, Time_mh depart);

        ~JourneyN();
        ConnectionN *c;

        vector <Journey*> journeis = {};
        Time_mh depart;
        Time_mh arrive;

    protected:

    private:
};



ostream& operator<<(ostream& os, JourneyN&  j);

bool sort_arrival(JourneyN* a, JourneyN* b);

namespace f{

//void print_journey(vector<JourneyN*> vec,  int i_max, ostream &os);


}




#endif // JOURNEYN_H
