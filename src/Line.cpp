#include "Line.h"
#include "functions2.h"

Line::Line()
{
    //ctor


}

Line::~Line()
{
    //dtor
}

Line::Line(int _number, vector <Stop*> _StopList, int _shield_number)
{
    number = _number;
    StopList = _StopList;
    shield_number = _shield_number;
}


Time_mh Line::depart(Time_mh& time, string& stop)
{
    int index = find_stop(stop);
    for(int i=0;i<routes.size();i++)
    {
     if((time<=(this->routes[i])->dep[index]))

           {
               //cout << "depart if" << endl;
               return (routes[i]->dep)[index];
           }
    }
    Time_mh t("99:22");
    return t;
}

Route* Line::get_route(Time_mh& time, Stop& stop )
{
    int index = find_stop(stop.Name);
    for(int i=0;i<routes.size();i++)
    {
     if((time<=(this->routes[i])->dep[index]))

           {
               //cout << "depart if" << endl;
               return (routes[i]);
           }
    }
    //Time_mh t("0:00");
    return nullptr;
}


 Time_mh Line::arrive(int route_num, Stop* s )
 {
     int index = find_stop(s->Name);
     return (routes[route_num])->dep[index];
 }

/** @brief (Gives number of index of stop in StopList)
  *
  * (documentation goes here)
  */
int Line::find_stop(string name)
{
    for(int i=0;i<StopList.size();i++)
    {
        if(name==(StopList[i])->Name)
            return i;
    }
    std::exit(156);
    return 99;
}

bool Line::connect (string a, string b)
{
    int start, endd;
    for(int i=0;i<StopList.size();i++)
    {
        if(a==(StopList[i])->Name)
        {
            start = i;
            break;
        }
    }
    for(int i=start;i<StopList.size();i++)
    {
        if(b==(StopList[i])->Name)
            return true;
    }
    return false;
}

bool Line::goes_through(Stop* _stop)
 {
     if(f::gett_index(StopList,_stop)==-1)
        return false;
     return true;
 }


