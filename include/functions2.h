#ifndef FUNCTIONS2_H
#define FUNCTIONS2_H


// *** ADDED BY HEADER FIXUP ***
#include <istream>
// *** END ***

#include "JourneyN.h"
#include <iostream>
#include <string>
#include "Stop.h"
#include <fstream>
#include <vector>
#include <sstream>
#include <algorithm>
//#include <JourneyN.h>


//using namespace std;


namespace f
{


template<typename T>
 void print_vector(vector<T> &vec, ostream &os)
 {
     for(int i=0;i<vec.size();i++)
        os << *vec[i] <<endl;
 }

 template<typename T>
    int gett_index(vector<T> vec, T object)
   {
        for(int i=0;i<vec.size();i++)
            if(vec[i]== object)
            return i;
      //cout <<"get index object not found" <<endl;
       return -1;
    }

void print_journey(vector<JourneyN*> vec,  int i_max, ostream &os);

}

#endif // FUNCTIONS2_H


