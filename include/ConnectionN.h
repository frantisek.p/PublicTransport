#ifndef CONNECTIONN_H
#define CONNECTIONN_H
#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <Connection.h>

using namespace std;


class ConnectionN
{
public:
    ConnectionN() {};
    vector <Connection*> con = {};
    friend ostream& operator << (ostream & os, const ConnectionN& c)
    {
        os << "stop numbers " << c.stop_numbers << endl;
        for(int i =0;i<c.con.size();i++)
        {
            os << *(c.con[i])<< endl ;
        }
        return os;
    }
    bool goes_through(Stop& _stop);
    int stop_numbers=0;
    void push_connection(Connection* c)
    {
        con.push_back(c);
        stop_numbers +=c->stop_numbers;
    }

private:

protected:

};

void remove_duplicate_connections(vector<ConnectionN*> &con);
bool same_lines(ConnectionN& a, ConnectionN& b);
bool operator == (ConnectionN& a, ConnectionN& b);


#endif




