#include "Stop.h"

using namespace std;

Stop::Stop()
{
    //ctor
}

Stop::~Stop()
{
    //dtor
}

Stop::Stop(const Stop& other)
{
    //copy ctor
}

Stop& Stop::operator=(const Stop& rhs)
{
    if (this == &rhs) return *this; // handle self assignment
    //assignment operator
    return *this;
}

Stop::Stop(string _name, int _id)
{
    Name = _name;
    Id = _id;

}

Stop::Stop(string _name)
{
    Name = _name;
    Id = 0;

}

void Stop::print_lines(ostream &os)
 {
            for(int i=0;i<line_numbers.size();i++)
                os << line_numbers[i] << endl;
}
