// *** ADDED BY HEADER FIXUP ***
#include <istream>
// *** END ***
#ifndef TIME_MH_H
#define TIME_MH_H

#include <cmath>
#include <iostream>
#include <string>

using namespace std;

class Time_mh
{
    public:
        /** Default constructor */
        Time_mh();
        Time_mh (int _h, int _m);
        Time_mh (int _allm);
        Time_mh (string _time);
        /** Default destructor */
        virtual ~Time_mh();
        int hours;
        int minutes;
        int all_minutes;
        friend Time_mh  operator + (const Time_mh& a, const Time_mh& b);
friend Time_mh  operator - (const Time_mh& a, const Time_mh& b);
friend  bool operator< ( Time_mh &,  Time_mh &);
friend  bool operator>
 ( Time_mh &,  Time_mh &);
friend  bool operator>= ( Time_mh &, Time_mh &);
friend  bool operator<= ( Time_mh &,  Time_mh &);
friend  bool operator== ( Time_mh &,  Time_mh &);
friend  bool operator!= ( Time_mh &,  Time_mh &);
friend ostream& operator<< (ostream& out, const Time_mh& num);

    protected:

    private:



};








#endif  //TIME_MH_H
