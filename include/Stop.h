#ifndef STOP_H
#define STOP_H



#include <string>
#include "Time_mh.h"
#include <vector>
//#include <Line.h>

using namespace std;


class Stop
{
    public:
        /** Default constructor */
        Stop();
        /** Default destructor */
        virtual ~Stop();
        /** Copy constructor
         *  \param other Object to copy from
         */
        Stop(const Stop& other);
        /** Assignment operator
         *  \param other Object to assign from
         *  \return A reference to this
         */
        Stop& operator=(const Stop& other);
        Stop( string, int);
        Stop(string Name);
         std::string Name;
        int Id;
        std::vector <int> line_numbers = {};
        Time_mh change_time = Time_mh (0, 2);
        bool change_stop = 1;

        void print_lines(ostream &os);




    protected:

    private:



};

inline bool operator == (Stop& a, Stop& b)
{
    if(a.Name == b.Name && a.Id == b.Id)
        return true;
    else
        return false;
}

#endif // STOP_H



