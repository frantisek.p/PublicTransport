#ifndef LINE_H
#define LINE_H

#include <Stop.h>
#include <vector>
#include <vector>
#include <Route.h>
#include <Time_mh.h>
//#include "functions2.h"


using namespace std;


class Line
{
    public:
        /** Default constructor */
        Line();
        /** Default destructor */
        virtual ~Line();
        Line(int _number, vector <Stop*> _StopList, int _shield_number);
        int number;
        vector <Stop*> StopList;
        int shield_number = number/100;
        vector <Route*> routes;
        Time_mh depart(Time_mh& time, string& stop);
        Time_mh arrive(int route_num, Stop* s );
        int find_stop(string Name);
        bool connect (string a, string b);
        bool goes_through(Stop* _stop);
        Route* get_route(Time_mh& time, Stop& stop );
        //bool connect (int a;int b);
        //bool connect (Stop& a; Stop& b);

    protected:

    private:

};

#endif // LINE_H
