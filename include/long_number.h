
#include <iostream>
#include <cmath>
#include <vector>
#include <typeinfo>
#include <string>
#include <cassert>
//#include "long_number.hh"





using namespace std; 

namespace ln
{
	
class long_number;
long_number pow2(const long_number &, const long_number &);

//stores long integers in vector, 0th position means 10^0 -> reversed order, using for operation of huge nubers overnight 
class long_number
{
private:



public:
vector<unsigned int> numero = {};//every component store one digit of the number
unsigned int size = 0;//size of the numero - how many digit has the number

long_number() {};//default constructor


void zero_check()//checks and remuves zero digits from the left of the number
{
	while(numero[numero.size()-1]==0&&numero.size()>=2)
	{
		numero.pop_back();
		size--;
	}
	size = numero.size();
}

void rotate()// means that once is on tenth etc, problem when zero(s)
{
	long_number a = 0;
	long_number ten = 10;
	for(int i=1;i<size;i++)
	a += this->numero[i-1] *  pow2((long_number)10,(long_number) i);
	a.numero[0] += (unsigned int) this->numero[size-1];
	*this = a;
}


explicit operator unsigned int() const //conversion form long_number to int, if neccessary of implicit conversion remove the word explicit
{  
	//cout << "typecast operator unsigned int() used" << endl;
	unsigned int num=0;
	for(unsigned int i=0;i<size;i++)
		num += (unsigned int) numero[i]*pow(10.,i);
	return num;
}		


template<typename numb>
long_number(numb a)//constructor
{
	//cout <<"template constructor" << endl;
	string b = to_string(a);
	read(b);
}

long_number( const long_number& a)//copy constructor
{
	numero = a.numero;
	size = a.numero.size();
}

int ssize() const
{
	return (this->numero).size();
}


void set_size(int i)
{
	(this->numero).resize(i);
}
void get (ostream& out) const;//historic function to print the long_number
void read(string input); //historic function to declare long_number
const vector <unsigned int>& get_numero() const //not necessary cauz all is public
{
	//*this = nume
	return numero;
}
void set_numero(int num,int pos)
{
numero[pos]=num;
}

unsigned int & operator[](const unsigned int i)
{
	assert(i<numero.size() && i>=0);
	return numero[i];
}
/*
long_number & operator[](const unsigned int i) const
{
	assert(i>=numero.size()|| i<0);
	return (long_number) numero[i];
}
*/
long_number& operator /=(const long_number& b)
{
	//long_number first=*this;
	//long_number sec = b;
	*this = *this/b;
	return *this;
}


long_number& operator -=(const long_number& b)
{
	long_number sec = b;
	if(sec<*this)
	{
		
		*this = (*this - sec);
	}
	else
		*this = (sec - *this);
	
	
	return *this;
}

long_number& operator+=(const long_number& b)//defined by the binary operator +
{
	long_number a= *this;
	*this = (*this + b);
	return *this;
}


long_number& operator*=(const long_number& b)
{
	*this = *this *b;
	return *this;
}

template <typename numb>
long_number& operator=( numb b)// I don't know why is this here
{
	///cout << "assign op called" <<endl;
	string s = to_string(b);
	this->read(s);
	return *this;

}

/*
template <typename numb>
long_number* operator=( numb b)
{
	string s = to_string(b);
	this.read(s);
	return this;

}
*/

 //friend ostream& operator<<(ostream& os, long_number& num);

~long_number(){}//destructor

//this is here to get acces inside the operators to another operators
//it should work also without this according to
//https://www.learncpp.com/cpp-tutorial/9-2a-overloading-operators-using-normal-functions/
//because all is public but it doesnt
friend inline bool operator>( const long_number &a,const long_number &b);
friend long_number operator - (const long_number&, const long_number&);
friend long_number operator + ( const long_number&, const long_number&);
friend inline bool operator< (const long_number &, const long_number &); 
friend inline bool operator>= (const long_number &,const long_number &); 
friend inline bool operator<= (const long_number &, const long_number &);
friend inline bool operator== (const long_number &, const long_number &); 
friend inline bool operator!= (const long_number &, const long_number &); 
friend long_number operator++(long_number &b,int i);
friend long_number operator *(const long_number &a,const long_number& b);
friend long_number operator /(const long_number &a,const long_number& b);
//friend long_number pow2(long_number & num, long_number & exp);


};

long_number operator+ (const long_number& a, const long_number& b)//sum of two long_numbers
{   
/*	long_number res;
	int a_size=a.get_numero().size();//size of number a
	int b_size=b.get_numero().size();//size of number b
	res.set_size(max(a_size,b_size)+1);
	int rest = 0; //stores the rest when adding over ten
	for(int i=0;i<min(a_size,b_size);i++)//part where both numbers have digits
	{
		res.set_numero((a.get_numero()[i]+(b.get_numero())[i]+rest)%10,i);//sum of digits,
		rest=((a.get_numero())[i]+(b.get_numero())[i]+rest)/10;// rest is saving suming over 10

	}
	if(a_size>b_size)//to find which number is greater
	for(int i=min(a_size,b_size);i<a_size;i++)//part where one numbers has no digit -> is less
	{
		res.get_numero().push_back(0);//set zero at the higher positions of the smallest number
		res.set_numero((a.get_numero()[i]+rest)%10,i);
		rest=((a.get_numero())[i]+rest)/10;
	}	
	else
	for(int i=min(a_size,b_size);i<b_size;i++)
	{
		res.get_numero().push_back(0);
		res.set_numero(((b.get_numero())[i]+rest)%10,i);
		rest=((b.get_numero())[i]+rest)/10;
	}
	if(rest!=0)//if the result has more digit than the greatest partner
	{
		res.get_numero().push_back(0);
		res.set_numero(rest,max(a_size,b_size));
	}
	if((res.numero[(res.numero).size()-1])==0)//reduces zero digits from left if possible
		(res.numero).erase((res.numero).end()-1);*/
	long_number res;
	int ten = 10;
	int a_size=a.get_numero().size();//size of number a
	int b_size=b.get_numero().size();//size of number b
	res.set_size(max(a_size,b_size)+1);
	int rest = 0; //stores the rest when adding over ten
	for(int i=0;i<min(a_size,b_size);i++)//part where both numbers have digits
	{
		res.set_numero((a.get_numero()[i]+(b.get_numero())[i]+rest)%ten,i);//sum of digits,
		rest=((a.get_numero())[i]+(b.get_numero())[i]+rest)/ten;// rest is saving suming over 10

	}
	if(a_size>b_size)//to find which number is greater
	for(int i=min(a_size,b_size);i<a_size;i++)//part where one numbers has no digit -> is less
	{
		//res.get_numero().push_back((a.get_numero()[i]+rest)%10);//set zero at the higher positions of the smallest number
		res.set_numero((a.get_numero()[i]+rest)%ten,i);
		rest=((a.get_numero())[i]+rest)/ten;
	}	
	else
	for(int i=min(a_size,b_size);i<b_size;i++)
	{
		//res.get_numero().push_back(((b.get_numero())[i]+rest)%10);
		res.set_numero(((b.get_numero())[i]+rest)%ten,i);
		rest=((b.get_numero())[i]+rest)/ten;
	}
	if(rest!=0)//if the result has more digit than the greatest partner
	{
		//res.get_numero().push_back(rest);
		res.set_numero(rest,max(a_size,b_size));
	}
	if((res.numero[(res.numero).size()-1])==0)//reduces zero digit from left if possible
		(res.numero).erase((res.numero).end()-1);
	res.zero_check();
	
return res;
}


ostream& operator<< (ostream& out, const long_number& num)//output of the number
{
	for(int i=(num.numero).size()-1;i>=0;i--)
	{
		out << (num.numero)[i];
	}
	return out;
}

/*
ostream& operator<< (ostream& out, long_number& num)
{
	for(int i=(num.numero).size()-1;i>=0;i--)
	{
		out << (num.numero)[i];
	}
	return out;
}
*/


/*
void operator<< (ostream& out, long_number& num)
{
	for(int i=(num.numero).size()-1;i>=0;i--)
	{
		out << (num.numero)[i];
	}
	
}
*/


long_number operator- (const long_number& a,  const long_number& b)
{
	long_number res = 0;
	long_number first;//horni cinitel - cislo, od ktereho je odcitano
	long_number sec;//dolni cinitel - cislo co se odcita
	if(a<b)
	{
		first = b;
		sec = a;
		res.set_size(b.numero.size());
	}
	else
	{
		first = a;
		sec = b;
		res.set_size(a.numero.size());
	}
	unsigned int i=0;
	unsigned int zb=0;
	while (i<sec.size)
	{
		if(first.numero[i]<(sec.numero[i]+zb))
		{
			res.numero[i]= (first.numero[i]+10)-(sec.numero[i]+zb);
			zb =1;
			i++;
			
		}
		else
		{
			res.numero [i] = first.numero[i] - (sec.numero[i]+zb);
			zb=0;
			i++;
		}	
	}
	for(unsigned int j=i;j<first.size;j++)
	{
		if(first.numero[j]<(0+zb))
		{
			res.numero[j]= (first.numero[j]+10)-(0+zb);
			zb =1;
		}
		else
		{
			res.numero [j] = first.numero[j] - (0+zb);
			zb=0;
		}
	}
	res.size = res.numero.size();
return res;
}

inline bool operator>( const long_number &a,const long_number &b)
{
	int a_size = a.numero.size();
	int b_size = b.numero.size();
	if(a_size>b_size)
	{
		return true;
	}
	else if(a_size<b_size)
	{
		return false;
	}
	else if(a_size==b_size)
		for(int i=(a_size-1);i>=0;i--)
		{
			if(a.numero[i]>b.numero[i])
			{
			return true;
			}
			else if(a.numero[i]<b.numero[i])
			{
				return false;
			}
		}
		return false;
}
		
inline bool operator< (const long_number &a, const long_number &b) {return (b>a);}
inline bool operator>= (const long_number &a, const long_number &b) {return !(a<b);}
inline bool operator<= (const long_number &a, const long_number &b) {return !(a>b);}
inline bool operator== (const long_number &a, const long_number &b) {return ((b<=a)&&(a<=b));}	
inline bool operator!= (const long_number &a, const long_number &b) {return !(b==a);}
/*
long_number operator *(int a, long_number &b)
{
	long_number res;
	res.read("0");
	for(int i=1;i<=a;i++)
		res += b;
	return res;
}*/

//inline long_number operator *(long_number &b, int a) {return a*b;}

//inline long_number operator *(long_number &a, long_number &b)
//{
	
long_number operator *(const long_number &a, const long_number& b)
{
	long_number res=0;
	res.numero = vector<unsigned int> (a.numero.size()+b.numero.size(),0);
	for(int i=0; i<a.ssize();i++)
		for(int j=0;j<b.ssize();j++)
		{
			res.numero[i+j] += a.numero[i]*b.numero[j];
		}
	int rest = 0;
	for(int i=0;i<res.ssize();i++)
	{
		int temp = res.numero[i];
		res.numero[i] = (res.numero[i]+rest)%10;
		rest = (temp+rest)/10;
	}
	while(res.numero[res.ssize()-1]==0 && res.numero.size()>=2)
		res.numero.pop_back();
	res.size = res.numero.size();
	return res;
}
	/*

long_number sum(const long_number a, const long_number b)
{
	long_number res;
	int a_size=a.get_numero().size();
	int b_size=b.get_numero().size();
	//cout << 48 << endl;
	res.set_size(max(a_size,b_size)+1);
	
	//cout << 41 << endl;
	int rest = 0; //add over ten
	for(int i=0;i<min(a_size,b_size);i++)
	{
		//cout << 45 <<endl;
		//cout << (a.get_numero())[i] << endl;
		res.set_numero(((a.get_numero())[i]+(b.get_numero())[i]+rest)%10,i);
		rest=((a.get_numero())[i]+(b.get_numero())[i]+rest)/10;
		//cout << (a.get_numero())[i] << endl;
	}
	//cout << 50 <<endl;
	if(a_size>b_size)
	for(int i=min(a_size,b_size);i<a_size;i++)
	{
		res.get_numero().push_back(0);
		res.set_numero(((a.get_numero())[i]+rest)%10,i);
		rest=((a.get_numero())[i])/10;
	}	
	else
	for(int i=min(a_size,b_size);i<b_size;i++)
	{
		res.get_numero().push_back(0);
		res.set_numero(((b.get_numero())[i]+rest)%10,i);
		rest=((b.get_numero())[i])/10;
	}
	if(rest!=0)
	{
		res.get_numero().push_back(0);
		res.set_numero(rest,max(a_size,b_size));
	}
	if((res.numero[(res.numero).size()-1])==0)
		(res.numero).erase((res.numero).end()-1);
	
return res;
}*/

void long_number::read(string input)
{
	//if(this->numero)
		this->numero.erase(this->numero.cbegin(),this->numero.cend());
	string s;
	if(typeid (input)== typeid (string))
		string s = input;
	else
		;
	//string s = to_string(input);
	for(int i=input.size()-1;i>=0;i--)
		(this->numero).push_back(input[i]-'0');
	this->size = (this->numero).size();
}

void long_number::get(ostream& out) const
{
	for(int i=(this->numero).size()-1;i>=0;i--)
	{
		out << (this->numero)[i];
	}
	//out << endl;
}

long_number pow2(const long_number & num, const long_number & exp)
{
	long_number res = num;
	for(long_number i=2;i<=exp;i++)
		res*=num;
	if(exp==(long_number)0)
		return 0;
	return res;
}


double sum(auto a, auto b)
{
	return a+b;
}

long_number operator++(long_number &b)
{
	long_number a;
	a.read("1");
	b = b+a;
	return b;
}

long_number operator++(long_number &b,int i)
{
	long_number temp = b;
	++b;
	
	return temp;
}

long_number operator--(long_number &b)
{
	long_number a;
	a.read("1");
	b = b-a;
	return b;
}

long_number operator--(long_number &b,int i)
{
	long_number temp = b;
	--b;
	return temp;
}
/*
template <typename numb>
long_number::long_number operator=( long_number& a, numb b)
{
	long_number res;
	string s = to_string(b);
	a->read(s);
	return a;

}*/

//long_number::long_number();
vector<long_number> divide( const long_number& a,const long_number& b)//first is the result, second is the rest
{
	
	long_number zero = 0;
	vector<long_number> result = {zero,a};
	//long_number null=0;
	//long_number *res = new long_number();
	//long_number *rest = new long_number();
	//result.push_back(res);
	//result.push_back(rest);
	long_number temp = a;
	long_number ten = 10;
	if(a<b)
	{
		result[0] = 0;
		result[1] = a;
		return result;
	}
	result[0]=0;
	while((temp>=b) && (result[0]<ten))
	{
		temp =temp - b;
		(result[0])++;
		temp.zero_check();
	}
	result[1]=temp;
	if(result[0]>=ten)
		cout <<"Not possible to use divide function!"<<endl;
	return result;
}
long_number operator /(const long_number &a,const long_number& b)
{
	long_number rev_result;
	long_number ten = 10;
	long_number zero = 0;
	vector<long_number> res = {zero, zero};
	for(int i=a.numero.size()-1;i>=0;i--)
	{
		long_number help =a.numero[i];
		(res[1]) = (res[1]) + help;
		res = divide((res[1]), b);
		rev_result.numero.push_back((unsigned int) (long_number) res[0]);
		//rev_result.numero[0] = res[0];
		//cout << "rev result numero " << rev_result.numero[0] << endl;
		res[1]= res[1]*ten;
	}
	long_number result;
	result.numero.resize(rev_result.numero.size());
	int j=0;
	for(int i=rev_result.numero.size()-1;i>=0;i--)
	{
		
		//result.numero.push_back(rev_result.numero[i]);
		result.numero[j] = rev_result.numero[i];
		j++;
		
	}
	result.zero_check();	
	result.size = result.numero.size();
	return result;
}

long_number operator% (const long_number &a, const long_number &b)
{
	long_number rev_result;
	long_number ten = 10;
	long_number zero = 0;
	long_number zero2 = 0;
	vector<long_number> res;
	res.push_back(zero);
	res.push_back(zero2);
	for(int i=a.numero.size()-1;i>=0;i--)
	{ 
		long_number help =a.numero[i];
		(res[1]) = (res[1]) +  help;
		res = divide((res[1]), b);
		long_number num = (res[0]);
		rev_result.numero.push_back((unsigned int)num);
		//cout << 595 <<endl;
		//cout << *(res[1]) <<endl;
		if(i!=0)
		(res[1])= ((res[1]))*ten;
		//cout << *(res[1]) <<endl;
	}
	long_number result;
	for(int i=rev_result.numero.size()-1;i>=0;i--)
	{
		result.numero.push_back(rev_result.numero[i]);
	}
	result.zero_check();
	long_number resultat = (res[1]);
	resultat.zero_check();
	resultat.size = resultat.numero.size();
	return resultat;
}


long_number factorial(const long_number &a)
{
	if(a==0)
		return (long_number) 1;
	long_number res=1;
	for(long_number i=1;i<=a;i++)
		res *= i;
	return res;
}

	
	
}


  


